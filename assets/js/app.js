'use strict'

// Jquery function, load when index loads
$(document).ready(function() {

    var store = store || {};
    // printStorage();

    if (window.sessionStorage.accessToken && window.sessionStorage.userId) {
        // Populate username:
        if ($('#username').length) {
            $('#username').text(window.sessionStorage.userName);
        }

        // Populate grade:
        var grade = $('#grade');
        if (grade.length) {
            grade.text(window.sessionStorage.grade + ' semestre');
        }

        // Populate the fullname:
        var fullname = $('#fullname');
        if (fullname.length) {
            fullname.text(window.sessionStorage.name + ' ' + window.sessionStorage.surName);
        }

        var detallesnombre = $('#detallesnombre');
        if (detallesnombre.length) {
            detallesnombre.text(window.sessionStorage.name + ' ' + window.sessionStorage.surName);
        }

        var detallesgrade = $('#detallesgrade');
        if (detallesgrade.length) {
            detallesgrade.text(window.sessionStorage.grade + ' semestre');
        }

        var pendingorders = $('#pendingorders');
        if (pendingorders.length) {

            console.log('About to check if the user has any orders ... ');
            printStorage();

            $.ajax({
                url: 'http://localhost:3977/api/user/hasorders/' + window.sessionStorage.userId,
                type: 'GET',
                // dataType: "jsonp",
                // crossDomain: false,
                headers: {
                    'Authorization': window.sessionStorage.accessToken,
                },
                success: function(data) {
                    console.log(data);
                    if (data.hasorders) {
                        var pendingOrders = $('#pendingorders');
                        pendingOrders.text('Ordenes pendientes');
                    } else {
                        pendingOrders.text('OK');
                    }
                },
                error: function(errorData) {
                    console.log('There was an error checking if user has orders ... ');
                    console.log(errorData);
                    console.log(errorData.responseJSON.message);
                }
            });
        }

        var cursosActuales = $('#cursosactuales');
        if (cursosActuales.length) {
            console.log('Cursos actuales found ... ');
            $.ajax({
                url: 'http://localhost:3977/api/user/courses/' + window.sessionStorage.userId,
                type: 'GET',
                headers: {
                    'Authorization': window.sessionStorage.accessToken,
                },
                success: function(data) {
                    console.log('OK :D');
                    var courses = data.course;
                    for (var i = 0; i < courses.length; i++) {
                        var course = courses[i];
                        var teacher = courses[i].teacher;

                        var materiaRow = '<tr><th scope="row">Materia</th>' + '<td class="table-font"><b>' + course.name + '</b></td></tr>';
                        var maestroRow = '<tr><th scope="row">Maestro</th><td class="table-font">' + teacher.name + '</td></tr>';
                        var semestre = '<tr><th scope="row">Grado</th><td class="table-font">' + course.grade + ' Semestre</td></tr>'

                        cursosActuales.append(materiaRow);
                        cursosActuales.append(maestroRow);
                        cursosActuales.append(semestre);
                    }
                },
                error: function(errorData) {
                    console.log('There was an error checking user courses.');
                    console.log(errorData);
                    console.log(errorData.responseJSON.message);
                }
            });
        }

        var listTabPedidosActivos = $('#list-tab-pedidos-activos');
        if (listTabPedidosActivos.length) {
            $.ajax({
                url: 'http://localhost:3977/api/userorders/' + window.sessionStorage.userId,
                type: 'GET',
                headers: {
                    'Authorization': window.sessionStorage.accessToken,
                },
                success: function(orders) {
                    if (orders) {
                        console.log('Orders: %c%d', 'color:blue', orders.length);
                        for (var i = 0; i < orders.length; i++) {
                            var order = orders[i];
                            var practice = order.practice;
                            var practiceName = practice.name;
                            console.log('Practica: %c%s - %s', 'color:blue', practiceName, practice._id);
                            var orderItemInfo = '<a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center" ';
                            orderItemInfo += 'href="order-info.html?orderId=' + order._id;
                            orderItemInfo += '" role="tab">' + practiceName + '<span class="fas fa-caret-right"></span></a>';

                            listTabPedidosActivos.append(orderItemInfo);
                        }
                    }
                },
                error: function(errorData) {
                    console.log('There was an error retrieving user orders ... ');
                    console.log(errorData);
                    console.log(errorData.responseJSON.message);
                }
            });
        }

        var orderInfoTable = $('#order-info-table');
        if (orderInfoTable.length) {

            const urlParams = new URLSearchParams(window.location.search);
            const orderIdParam = urlParams.get('orderId');

            if (orderIdParam) {
                console.log("Order: %c%s\n", 'color:red', orderIdParam);
                $.ajax({
                    url: 'http://localhost:3977/api/orderbyid/' + orderIdParam,
                    type: 'GET',
                    headers: {
                        'Authorization': window.sessionStorage.accessToken,
                    },
                    success: function(order) {
                        if (order) {
                            var practice = order.practice;
                            var practiceName = practice.name;
                            console.log('Practica: %c%s - %s', 'color:blue', practiceName, practice._id);
                            var course = practice.course;
                            var teacher = course.teacher;

                            $('#materia').text(course.name);
                            $('#maestro').text(teacher.name);
                            $('#practiceName').text(practice.name);

                            var materials = practice.material;
                            var practiceMaterials = $('#practiceMaterials');
                            for (var i = 0; i < materials.length; i++) {
                                var material = materials[i];
                                var cantidad = material.stock;
                                var materialName = material.material.name;
                                var materialRow = '<tr><th scope="row">' + materialName + '</th><td class="table-font">' + cantidad + '</td></tr>';
                                practiceMaterials.append(materialRow);
                            }

                            console.log(order.reqDate);
                            var date = new Date(order.reqDate * 1000);

                            var dateString = date.toLocaleDateString();
                            var dateTokens = dateString.split("/");

                            var ddmmyyyy = pad(dateTokens[1]) + "/" + pad(dateTokens[0]) + "/" + dateTokens[2];

                            $('#exp-date').text('Pedido en ' + ddmmyyyy + ' a las ' + date.toLocaleTimeString());

                        }
                    },
                    error: function(errorData) {
                        console.log('There was an error retrieving user order ... ');
                        console.log(errorData);
                        console.log(errorData.responseJSON.message);
                    }
                });
            } else {
                console.log('%cOrder param Id not found ... ', 'color:red')
            }

        }

        var practiceDeck = $('#practice-deck');
        if (practiceDeck.length) {
            console.log('Practice deck found ...');
            $.ajax({
                url: 'http://localhost:3977/api/practices',
                type: 'GET',
                headers: {
                    'Authorization': window.sessionStorage.accessToken,
                },
                success: function(practices) {
                    if (practices) {

                        var practiceDeckTemplate = `
                        <div class="card hvr-shrink">
                            <a href="practice.html?practiceToOrderId=@id@">
                                <div class="baner"><h2><b>@practiceName@</b></h2></div>
                                <div class="card-body">
                                    <h2 class="card-title"><b>@coursename@</b></h2>
                                </div>
                            </a>
                        </div>
                        `;

                        console.log('Practices: %c%d', 'color:blue', practices.length);
                        for (var i = 0; i < practices.length; i++) {
                            var practice = practices[i];
                            var id = practice._id;
                            var practiceName = practice.name;
                            var courseName = practice.course.name;
                            console.log('Practica: %c%s - %s', 'color:blue', practiceName, practice._id);

                            var deck = practiceDeckTemplate.replace('@practiceName@', practiceName);
                            deck = deck.replace('@coursename@', courseName);
                            deck = deck.replace('@id@', id);

                            practiceDeck.append(deck);
                        }
                    }
                },
                error: function(errorData) {
                    console.log('There was an error retrieving user orders ... ');
                    console.log(errorData);
                    console.log(errorData.responseJSON.message);
                }
            });
        }

        var practiceInfoToOrder = $('#practiceInfoToOrder');
        if (practiceInfoToOrder.length) {
            const urlParams = new URLSearchParams(window.location.search);
            const practiceIdParam = urlParams.get('practiceToOrderId');
            if (practiceIdParam) {
                $.ajax({
                    url: 'http://localhost:3977/api/practicebyid/' + practiceIdParam,
                    type: 'GET',
                    headers: {
                        'Authorization': window.sessionStorage.accessToken,
                    },
                    success: function(practice) {
                        console.log(practice);
                        var materiales = practice.material;
                        var practiceName = practice.name;
                        var courseName = practice.course.name;
                        var teacherName = practice.course.teacher.name;
                        console.log('Teacher name: ' + teacherName);
                        $('#courseName').text(courseName);
                        $('#practiceName').text(practiceName);
                        $('#teacherName').append($('<img src="./assets/img/default-avatar.png" alt="" class="rounded-circle" height="20">Maestro(a): ' + teacherName + '</img>'));

                        var materiales = practice.material;

                        var materialInPactice = $('#materialInPactice');
                        for (var i = 0; i < materiales.length; i++) {
                            var materialTemplate = `
                            <tr>
                                <th scope="row">@materialName@</th>
                                <td class="table-font">@cantidad@</td>
                            </tr>`;

                            var material = materiales[i];
                            materialTemplate = materialTemplate.replace('@materialName@', material.material.name);
                            materialTemplate = materialTemplate.replace('@cantidad@', material.stock);

                            materialInPactice.append(materialTemplate);
                        }

                    },
                    error: function(errorData) {
                        console.log('There was an error checking user courses.');
                        console.log(errorData);
                        console.log(errorData.responseJSON.message);
                    }
                });
            } else {
                console.log('%cOrder param Id not found ... ', 'color:red')
            }
        }

    }

    // Sets the jwt to the store object
    store.setJWT = function(token, user) {
        window.sessionStorage.accessToken = token;
        window.sessionStorage.userId = user._id;
        window.sessionStorage.userName = user.username;
        window.sessionStorage.surName = user.surname;
        window.sessionStorage.name = user.name;
        window.sessionStorage.grade = user.grade;
        printStorage();
    }

    new WOW().init();

    $('#myModal').on('shown.bs.modal', function () {
        document.getElementById("modalText").innerHTML = "cACA";
        $('#myInput').trigger('focus');
    });
   /* $("#btn-ok").click(function() { window.confirm(); });*/

    $('#loginform').on('submit', function(e) {
        e.preventDefault();

        var username = $('#username').val();
        var password = $('#password').val();

        $.ajax({
            url: 'http://localhost:3977/api/login',
            type: 'POST',
            data: {username: username, password: password, gethash: true},
            success: function(data) {
                console.log('OK ... token received ... ');
                store.setJWT(data.token, data.user);
                goHome();
            },
            error: function(errorData) {
                console.log('There was an error login in ... ');
                console.log(errorData.responseJSON.message);
            }
        });

    });

});

function confirmBtn() {
    location.replace("order.html");
    /*var txt;
    if(confirm("Confirmar pedido?")){
        location.replace("order-info.html");
    }else{

    }*/
}

function confirmLogOut() {
    window.sessionStorage.clear();
    location.replace("login.html");
}

function goHome() {
    location.replace("index.html");
}

function printStorage() {
    console.log('~~~~~~> Storage <------');
    console.log(window.sessionStorage.accessToken);
    console.log(window.sessionStorage.userId);
    console.log(window.sessionStorage.userName);
    console.log(window.sessionStorage.surName);
    console.log(window.sessionStorage.name);
    console.log(window.sessionStorage.grade);
    console.log(window.sessionStorage);
    console.log('~~~~~~> Storage END <------');
}

function pad(n) {
    return n < 10 ? ('0'+n) : n;
}
